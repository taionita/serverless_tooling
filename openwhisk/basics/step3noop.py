from datetime import datetime
import sys

def main(args):
    print("id[" + str(args["id"]) + "]--step3-start" + "  --" + str(datetime.now())  + "--")
    print("id[" + str(args["id"]) + "]--step3-end" + "    --" + str(datetime.now())  + "--" )
    return {"next_step": "done", "id": args["id"]}

if __name__ == '__main__': 
    main({'id': 93})
