from datetime import datetime
import sys

def main(args):
    print("id[" + str(args["id"]) + "]--step3-start" + "  --" + str(datetime.now())  + "--")
    n = datetime.now()
    while ((datetime.now() - n).total_seconds() * 1000) < 500: # spin for 500ms
        continue
    print("id[" + str(args["id"]) + "]--step3-end" + "    --" + str(datetime.now())  + "--")
    return {"next_step": "done", "id": args["id"]}

if __name__ == '__main__': 
    main({'id': 103})
