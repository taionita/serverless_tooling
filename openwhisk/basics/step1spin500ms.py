from datetime import datetime
import sys

def main(args):
    print("id[" + str(args["id"]) + "]--step1-start" + "  --" + str(datetime.now())  + "--" )


    n = datetime.now()
    while ((datetime.now() - n).total_seconds() * 1000) < 500: # spin for 500ms
        continue
    print("id[" + str(args["id"]) + "]--step1-end" + "    --" + str(datetime.now())  + "--" )

    return {"next_step": "step2", "id": args['id']}


if __name__ == '__main__':
    main({"id": 101})
