from datetime import datetime
import sys

def main(args):
    print("id[" + str(args["id"]) + "]--step2-start" + "  --" + str(datetime.now())  + "--")
    print("id[" + str(args["id"]) + "]--step2-end" + "    --" + str(datetime.now())  + "--")
    return {"next_step": "step3", "id": args["id"]}


if __name__ == '__main__': 
    main({'id': 92})
