#!/usr/bin/python3

import sys, json, subprocess, time
from subprocess import check_output

def main():
    # reading activation id as args to the script
    if len(sys.argv) > 1:
        log_act_number = sys.argv[1:]
        print(log_act_number)

        for log in log_act_number:
            print(f"-- for activation id {log}, log is:")
            subprocess.run(f"./wsk -i  activation logs {log}", shell=True)

        return

    # reading from a pipe, like this 
    # time ./wsk -i action invoke basics/steps -p id 4224 -b | ./show_logs.py
    lines= []
    add = False
    for line in sys.stdin:
        if line == '{\n':
            add = True

        if add:
            lines.append(line)
        print(line, end='')
        if line == '}\n':
            add = False

    data = json.loads("".join(lines))
    log_act_number = data['logs']

    for index, log in enumerate(log_act_number):
        if index == 0:
            time.sleep(1) # give system time to collect the logs and shove them into the DB (result from the invoked function is returned possibly before the logs are collected)
        print(f"-- for activation id {log}, log is:")
        outputt = check_output(["./wsk", "-i", "activation", "logs", log]).decode(sys.stdout.encoding).strip()
        print(outputt)
        #subprocess.run(f"./wsk -i  activation logs {log}", shell=True)


if __name__ == '__main__':
    main()

