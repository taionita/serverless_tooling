#!/usr/bin/python

import sys
import json
import requests
import subprocess
import urllib3
from dateutil import parser
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def get_log_lines(log_id):
    auth_header = "Basic Nzg5YzQ2YjEtNzFmNi00ZWQ1LThjNTQtODE2YWE0ZjhjNTAyOmFiY3pPM3haQ0xyTU42djJCS0sxZFhZRnBYbFBrY2NPRnFtMTJDZEFzTWdSVTRWck5aOWx5R1ZDR3VNREdJd1A="
    url = "https://127.0.0.1:31001/api/v1/namespaces/_/activations/{}/logs".format(log_id)
    resp = requests.get(url,  headers={'Authorization': auth_header}, verify=False)
    logs = json.loads(resp.content)['logs']
    return logs[-2:]


def parse_time(str_must_be_there, logs):
    for log in logs:
        if str_must_be_there in log:
            tim = log.split(" ")[0]
            return parser.parse(tim)

    raise Exception("Coudln't find the log with '{}' among: {}".format(str_must_be_there, logs))

def main():

    if len(sys.argv) == 1:
        filename="/tmp/runner_results"
    else:
        filename=sys.argv[1]

    print("Opening file {} for results".format(filename))

    with open(filename) as fd:
        chain_runs = list(map(json.loads, fd.readlines()))

    diffs = {"1-2": [], "2-3": []}
    for chain_run in chain_runs:
        logs = []
        for log in chain_run['logs']:
            print("getting logs for function log id {}".format(log))
            logs += get_log_lines(log)

        s1end = parse_time('--step1-end', logs)
        s2start = parse_time('--step2-start', logs)
            
        s2end = parse_time('--step2-end', logs)
        s3start = parse_time('--step3-start', logs)
        diffs["1-2"].append(s2start - s1end)
        diffs["2-3"].append(s3start - s2end)
        
    
    print("Diffs from end of step 1 to start of step 2: {}".format( list(map(str, diffs["1-2"]))))
    print("Diffs from end of step 2 to start of step 3: {}".format( list(map(str, diffs["2-3"]))))


main()
