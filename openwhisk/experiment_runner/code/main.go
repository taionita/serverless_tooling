package main

import (
//  "errors"
  "io/ioutil"
  "fmt"
  "net/http"
   "os"
//    "reflect"
    "flag"
  "time"
 "bytes"
 "crypto/tls"
   "math/rand"
)




func InvokeChain(client *http.Client, req *http.Request) string {// *http.Response {
  //URL := "https://127.0.0.1:31001/api/v1/namespaces/_/actions/basics/chainnoop?blocking=true&result=false"

 
   
   resp, err := client.Do(req)
   if err != nil {
     fmt.Println(req.URL)
     fmt.Printf("http request error: %v\n", err)
     panic(err)
    }

   if resp.StatusCode != 200 {
     fmt.Printf("error code is not 200 for %s", req.URL)
     fmt.Printf("%v", resp)
     panic(resp)
   }

   body, err := ioutil.ReadAll(resp.Body)
   if err != nil { 
      fmt.Printf("coudln't read body %v: %v",  err, resp.Body)
      panic(err)
   }

  //var jsonBody map[string]interface{}
  //err = json.Unmarshal(body, &jsonBody)
   bodyStr := string(body)
   //fmt.Printf("%v", jsonBody["activationId"])
   //fmt.Printf("%v", jsonBody)
   

//   return resp
   resp.Body.Close()
   return bodyStr
}


func waitFor(dur *time.Duration) {
  time.Sleep(*dur)

}

func BuildHTTPClient() *http.Client {


  tr := &http.Transport{
        TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
    }
  client := &http.Client{Transport: tr}
  return client

}


func BuildHTTPReq(proto string, chainName string, port string) (*http.Request, int)  {
  auth := "Nzg5YzQ2YjEtNzFmNi00ZWQ1LThjNTQtODE2YWE0ZjhjNTAyOmFiY3pPM3haQ0xyTU42djJCS0sxZFhZRnBYbFBrY2NPRnFtMTJDZEFzTWdSVTRWck5aOWx5R1ZDR3VNREdJd1A=" // this is harcoded in the openwhisk cluster config and the whisk cli has it too
  URL := fmt.Sprintf("%s://127.0.0.1:%s/api/v1/namespaces/_/actions/%s?blocking=true&result=false", proto, port, chainName)

  expId := rand.Intn(60000)
  req, err := http.NewRequest("POST", URL, bytes.NewBuffer([]byte(fmt.Sprintf(`{"id":%d}`,expId))))
  if err != nil {
   panic(err)

  }
  req.Header.Add("Authorization","Basic " + auth)
  req.Header.Set("Content-Type", "application/json")

  return req, expId
}


func main(){

  numCallsFlag := flag.Int("numCalls", 3, "how many times to invoke the chain (excluding the first invocation)")
  interCallWaitFlag := flag.Duration("waitTime", 100 * time.Millisecond, "how long to wait between two back to back invocations")
  chainName := flag.String("chain", "", "name of chain to invoke, including namespace e.g. basics/chainnoop")
  port := flag.String("port", "31001", "port to connect to; OpenWhisk is usually \"31001\"")
  httpsUse := flag.Bool("https", true, "whether to use https or not (OpenWhisk should use https")
  resultFilePath := flag.String("resultFile", "/tmp/runner_results", "Where this program will dump the results for later processing")
  //expId := flag.String("experimentID", "", "Very optional, to help make logs clearer, if you want to pass some experiment ID")


  flag.Parse()

  if *chainName == "" {
   fmt.Printf("Need a chain name to invoke. Insert coin and try again\n")
  return
  }

  proto := "http"
  if *httpsUse {
    proto = "https"
  }


  numCalls := *numCallsFlag + 1 // extra call because cold starts

  fmt.Printf("The experiment, for %d calls (including an initial call for cold starts) with %v between each, should take about %v\n", numCalls, interCallWaitFlag, time.Duration(numCalls) * *interCallWaitFlag)

//   start := time.Now()
 //  elapsed := time.Since(start)
//   fmt.Printf("took %v", elapsed)

  experimentIDs := make([]int, numCalls)
  requests := make([]*http.Request, numCalls)
  responses := make([]string, numCalls)

  client := BuildHTTPClient()

  for i := 0; i < numCalls; i++ {
    req, expID := BuildHTTPReq(proto, *chainName, *port)
    requests[i] = req
    experimentIDs[i] = expID

 }

  for i := 0; i < numCalls; i++ {
     responses[i] = InvokeChain(client, requests[i])

  }

  f, err := os.OpenFile(*resultFilePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
  if err != nil {
      panic(err)
  }


  for i := 1; i < numCalls; i++ {
      _, err = f.WriteString(responses[i])
      if err != nil { panic(err) }

      _, err =  f.WriteString("\n")
      if err != nil { panic(err) }

   }




  if err := f.Close(); err != nil {
      panic(err)
  }

}
