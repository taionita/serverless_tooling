#!/bin/bash

set -e
set -u

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "${SCRIPT_DIR}"

./wsk -i package update basics
./wsk -i action update basics/step1noop basics/step1noop.py
./wsk -i action update basics/step2noop basics/step2noop.py
./wsk -i action update basics/step3noop basics/step3noop.py

./wsk -i action update basics/chainnoop --sequence basics/step1noop,basics/step2noop,basics/step3noop

./wsk -i action update basics/step1spin basics/step1spin500ms.py
./wsk -i action update basics/step2spin basics/step2spin500ms.py
./wsk -i action update basics/step3spin basics/step3spin500ms.py

./wsk -i action update basics/chainspin --sequence basics/step1spin,basics/step2spin,basics/step3spin

