#!/usr/bin/python3
from dateutil.parser import parse
import sys

lines = []
for line in sys.stdin:
    print(line, end="")
    lines.append(line)


end1 = list(filter(lambda x: "--step1-end" in x, lines))[0]
end1 = end1.split("--")[-2]
end1 = parse(end1)

start2 = list(filter(lambda x: "--step2-start" in x, lines))[0]
start2 = start2.split("--")[-2]
start2 = parse(start2)

end2 = list(filter(lambda x: "--step2-end" in x, lines))[0]
end2 = end2.split("--")[-2]
end2 = parse(end2)

start3 = list(filter(lambda x: "--step3-start" in x, lines))[0]
start3 = start3.split("--")[-2]
start3 = parse(start3)

print("delta::::s2-e1::",str(start2 - end1))
print("delta::::s3-e2::",str(start3 - end2))
