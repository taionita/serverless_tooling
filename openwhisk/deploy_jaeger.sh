#!/bin/bash

echo "additionally, common/scala/src/main/resources/application.conf needs to  be changed, the tracing zipkin part needs to be commented in"

#kubectl create deployment jaeger --image=jaegertracing/all-in-one -n openwhisk
kubectl create deployment jaeger --image=jaegertracing/all-in-one -n openwhisk -- /go/bin/all-in-one-linux --collector.zipkin.host-port=:9411
kubectl expose deployment jaeger --name=zipkin --type ClusterIP --port 9411,16686 -n openwhisk 

