#!/bin/bash

set -e
set -u

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "${SCRIPT_DIR}"

our_id=$RANDOM
# we invoke our chain of 3 steps and we pass it an id just to keep the logs clear
# the output of the chain is fed into show_logs.py because we want show_logs.py to parse that json output, get the log ids and ask openwhisk to show us the logs of the functions

#chainname="${1:-basics/chainnoop}"
chainname="basics/chainnoop"
nutiter="${1:-1}"
echo "Invoking once to warm up containers"
./wsk action invoke "${chainname}" -i -b -p id ${our_id}
rm -f /tmp/invoker_output
rm -f /tmp/logs_output
echo "Will invoke ${chainname} ${nutiter} times and pass it arg id ${our_id}"
i=1
while [ $i -le $nutiter ]; do
     echo "$i/$nutiter"
    i=$((i+1))
    ./wsk action invoke "${chainname}" -i -b -p id ${our_id} | ./show_logs.py 2>&1 | ./show_delta.py | tee -a /tmp/invoker_output
done

#./wsk action invoke "${chainname}" -i -b -p id ${our_id} | ./show_logs.py
#kubectl logs -n openwhisk owdev-controller-0 | grep "::Mark" > /tmp/logs_output
kubectl logs -n openwhisk owdev-controller-0 | grep "\"mark\"" > /tmp/logs_output
kubectl logs -n openwhisk $(kubectl get pods -n openwhisk | grep invoker | cut -f 1 -d ' ')  | grep "\"func\"" > /tmp/logs_output2
