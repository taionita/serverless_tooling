#!/bin/bash

echo ${SUDO_USER}
if [ "$EUID" -ne 0 ]
  then echo "Please run using sudo"
  exit
fi
set -e
set -u

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd ) 
cd "${SCRIPT_DIR}"


#rm -f /etc/containerd/config.toml
#systemctl restart containerd 

kubeadm init --pod-network-cidr=10.244.0.0/16
#kubeadm init --config ./kubeadm_init.config
echo "OK NOW HAVE THE OTHER NODES JOIN USING THE KUBE ADM JOIN COMMAND"
kubeadm token create --print-join-command 
