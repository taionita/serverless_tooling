#!/bin/bash

echo ${SUDO_USER}
if [ "$EUID" -eq 0 ]
  then echo "Please don't use sudo, you're not that great"
  exit
fi
set -e
set -u

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# after kubeadm init
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml

curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

echo IF YOU ARE INSTALLING OPENWHISK, LABEL A NODE AS AFFINIY FOR INVOKER WITH \(REPLACE NODE NAME HERE WITH EXISTING NODE NAME\) "kubectl label node node2.srvless.sds-pg0.apt.emulab.net openwhisk-role=invoker" 
