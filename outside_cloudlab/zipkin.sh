#!/bin/bash
set -u
set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )       
cd "${SCRIPT_DIR}"

k8scontroller=$(cat node0)

ssh -p 22 "${k8scontroller}" -L 9411:localhost:9411 -- kubectl port-forward svc/zipkin 9411:9411 --namespace openwhisk



