#!/bin/bash
set -e
set -u

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )       
cd "${SCRIPT_DIR}"
node2=$(cat node2)
ssh "${node2}"
