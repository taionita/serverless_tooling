#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd ) 
cd "${SCRIPT_DIR}"

set -e
set -u


k8s=$(cat node0) #k8s controller
controller=$(cat node1) #openwhisk controller
invoker=$(cat node2) #openwhisk invoker

# terminate openwhisk if it's running
STR=$(ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "sudo helm status -n openwhisk owdev 2>&1") || true
SUB="release: not found"

set +e
ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "kubectl delete -n openwhisk deploy jaeger"
ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "kubectl delete -n openwhisk svc zipkin"
set -e


if [[ "$STR" =~ .*"$SUB".* ]]; then
  echo "OpenWhisk already terminated"

else
 echo "Terminating OpenWhisk"
 ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "sudo helm uninstall -n openwhisk owdev"
 sleep 3
 ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "/users/AlexT/serverless_tooling/wait_openwhisk_terminated.py"
fi




# recompile locally on openwhisk controller and invoker
ssh -o StrictHostKeyChecking=no -p 22 "${controller}" -- "sudo /users/AlexT/serverless_tooling/recompile_openwhisk.sh" &
controller_pid=$!

ssh -o StrictHostKeyChecking=no -p 22 "${invoker}" -- "sudo /users/AlexT/serverless_tooling/recompile_openwhisk.sh" &
invoker_pid=$!

wait ${controller_pid}
wait ${invoker_pid}

sleep 2

# start up the openwhisk adventure again
ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "sudo /users/AlexT/serverless_tooling/openwhisk_init.sh"
sleep 3
ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "/users/AlexT/serverless_tooling/wait_openwhisk_init.py"

ssh -o StrictHostKeyChecking=no -p 22 "${k8s}" -- "/users/AlexT/serverless_tooling/openwhisk/deploy_jaeger.sh"






