#!/bin/bash
set -u
set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "${SCRIPT_DIR}"
logname=${1:-invoker_output}
k8scontroller=$(cat node0)

scp "${k8scontroller}":/tmp/logs_output .
scp "${k8scontroller}":/tmp/invoker_output .

