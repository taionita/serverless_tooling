#!/bin/bash
# Expecting cloudlab copy pasted ssh command to be passed as arg 
# Specifically something like
#./node_init.sh ssh AlexT@apt032.apt.emulab.net
set -u
set -e
host=$2
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )


node_name=$(ssh -o StrictHostKeyChecking=no -p 22 "${host}" -- "hostname")
cd "${SCRIPT_DIR}"
node_name=$(echo "${node_name}" | cut -d '.' -f 1)
echo "${host}" > "${node_name}"


# copt over gitlab private key
scp -o StrictHostKeyChecking=no  ~/.ssh/id_rsa_gitlab "${host}":/users/AlexT/.ssh/id_rsa_gitlab

# create an ssh config file to use that key we just copied over
ssh -o StrictHostKeyChecking=no -p 22 "${host}" -- "printf \"Host git.uwaterloo.ca\n  PreferredAuthentications publickey\n  IdentityFile /users/AlexT/.ssh/id_rsa_gitlab\n  StrictHostKeyChecking no\n\" > /users/AlexT/.ssh/config"

ssh -o StrictHostKeyChecking=no -p 22 "${host}" -- "sudo apt install -y git; git clone https://github.com/apache/openwhisk-deploy-kube.git /users/AlexT/openwhisk-deploy-kube ;git clone ist-git@git.uwaterloo.ca:taionita/serverless_tooling.git /users/AlexT/serverless_tooling; git clone ist-git@git.uwaterloo.ca:taionita/openwhisk_WASL.git /users/AlexT/openwhisk_WASL"

ssh -o StrictHostKeyChecking=no -p 22 "${host}" -- "sudo /users/AlexT/serverless_tooling/node_init1.sh"

printf -- "----\n----\n----\nDONE\n----\n----\n----\n"

