#!/bin/bash
set -u
set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )       
cd "${SCRIPT_DIR}"

k8scontroller=$(cat node0)

ssh -p 22 "${k8scontroller}" -L 9090:localhost:9090 -- kubectl port-forward svc/owdev-prometheus-server 9090:9090 --namespace openwhisk
#ssh -p 22 "${k8scontroller}" -L 9090:localhost:9090 



