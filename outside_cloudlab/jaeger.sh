#!/bin/bash
set -u
#set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )       
cd "${SCRIPT_DIR}"

k8scontroller=$(cat node0)

echo "port forwarding from localhost to kubernetes cluster for already running jaeger all in one"

ssh -p 22 "${k8scontroller}" -L 16686:localhost:16686  -- killall kubectl
ssh -p 22 "${k8scontroller}" -L 16686:localhost:16686  -- kubectl port-forward -n openwhisk svc/zipkin 16686:16686



