#!/bin/bash
set -e
set -u

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )       
cd "${SCRIPT_DIR}"
node1=$(cat node1)
ssh "${node1}"
