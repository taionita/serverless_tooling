#!/bin/bash
set -u
set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )       
cd "${SCRIPT_DIR}"

k8scontroller=$(cat node0)

ssh -p 22 "${k8scontroller}" -L 3000:localhost:3000 -- kubectl port-forward svc/owdev-grafana 3000:3000 --namespace openwhisk
#ssh -p 22 "${k8scontroller}" -L 9090:localhost:9090 



