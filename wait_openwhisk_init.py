#!/usr/bin/env python3
import subprocess
import time




def main():
    ticks = 0
    while True:
        time.sleep(1)
        ticks += 1
        if ticks % 30 == 0:
            print("Waiting for deployment to complete..")
        status = subprocess.getoutput('kubectl get pods -n openwhisk')
        status = status.split('\n')
        if 'NAME' not in status[0] and 'AGE' not in status[0]:
            print("ERROR: kubectl output doesn't make sense to this script. That output is:") 
            print(status)
            sys.exit(43)

        # check invoker status
        invoker = list(filter(lambda x: 'owdev-invoker' in x, status))
        if len(invoker) != 1:
            print("ERROR: found 0 or 2 more or invokers in kubectl output:")
            print(status)
            sys.exit(43)
        invoker = invoker[0].split()
        if invoker[2] != 'Running':
            continue

        controller = list(filter(lambda x: 'owdev-controller' in x, status)) 
        if len(controller) != 1:
            print("ERROR: found 0 or 2 more or controllers in kubectl output:")
            print(status)
            sys.exit(43)
        controller = controller[0].split()
        if controller[2] != 'Running':
            continue
        break
    time.sleep(10)
    print("OpenWhisk Helm deployment complete")


if __name__ == '__main__':
    main()
