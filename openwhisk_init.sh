#!/bin/bash

echo ${SUDO_USER}
if [ "$EUID" -ne 0 ]
  then echo "Please use sudo"
  exit
fi
set -e
set -u

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#cd ~
#git clone https://github.com/apache/openwhisk-deploy-kube.git


echo "changing imagepull policy in the helm chart in helm values.yaml"
cp ~/serverless_tooling/helm_config_files/helm/openwhisk/values.yaml ~/openwhisk-deploy-kube/helm/openwhisk/values.yaml


echo "changing idle container and pause grace in the helm chart in invoker-pod.yaml"
cp ~/serverless_tooling/openwhisk/invoker-pod.yaml  ~/openwhisk-deploy-kube/helm/openwhisk/templates/invoker-pod.yaml

#helm repo add openwhisk https://openwhisk.apache.org/charts
#helm repo update
#helm install owdev openwhisk/openwhisk -n openwhisk --create-namespace -f ${SCRIPT_DIR}/openwhisk/mycluster.yaml
helm install owdev ~/openwhisk-deploy-kube/helm/openwhisk -n openwhisk --create-namespace -f ${SCRIPT_DIR}/openwhisk/mycluster.yaml

# the helm uninstall: helm uninstall owdev -n openwhisk

${SCRIPT_DIR}/openwhisk/wsk property set --apihost 127.0.0.1:31001 --auth 789c46b1-71f6-4ed5-8c54-816aa4f8c502:abczO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP
echo "Wait for pod 'owdev-install-packages' to complete or fail, and then next step!"
