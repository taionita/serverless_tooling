#!/usr/bin/env python3
import subprocess
import time
import sys



def main():
    ticks = 0
    while True:
        time.sleep(1)
        ticks += 1
        if ticks % 30 == 0:
            print("Waiting for deployment to terminate..")
        status = subprocess.getoutput('kubectl get pods -n openwhisk')
        status = status.split('\n')
        if status[0] == 'No resources found in openwhisk namespace.':
            break

        if 'NAME' not in status[0] and 'AGE' not in status[0]:
            print("ERROR: kubectl output doesn't make sense to this script. That output is:") 
            print(status)
            sys.exit(43)


        if len(status) != 1:
            continue
        break

    print("OpenWhisk Helm terminated")

if __name__ == '__main__':
    main()
