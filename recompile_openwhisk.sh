#!/bin/bash

# https://github.com/apache/openwhisk/blob/master/ansible/README.md
# https://github.com/apache/openwhisk/blob/master/tools/ubuntu-setup/README.md
set -e
set -u

if [ "$EUID" -ne 0 ]
  then echo "Please run using sudo"
  exit
fi

cd /users/AlexT/openwhisk_WASL
git pull
# ./gradlew distDocker
# see all tasks
# sudo ./gradlew -q :tasks --all 

./gradlew core:controller:distDocker
./gradlew core:invoker:distDocker

sudo ctr -n k8s.io i remove docker.io/whisk/invoker:latest
sudo ctr -n k8s.io i remove whisk/invoker:latest

sudo ctr -n k8s.io i remove docker.io/whisk/controller:latest
sudo ctr -n k8s.io i remove whisk/controller:latest

docker save whisk/invoker -o /tmp/invoker.tar
ctr -n k8s.io image import /tmp/invoker.tar
rm -f /tmp/invoker.tar
ctr -n k8s.io i tag docker.io/whisk/invoker:latest whisk/invoker:latest

docker save whisk/controller -o /tmp/controller.tar
ctr -n k8s.io image import /tmp/controller.tar 
rm -f /tmp/controller.tar
ctr -n k8s.io i tag docker.io/whisk/controller:latest whisk/controller:latest
