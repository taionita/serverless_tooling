# Installation Steps (Note: nothing is reboot-safe)

## Set up CloudLab Cluster
(Maybe this matters: ATP Utah Cluster)
3 nodes, Ubuntu 18.04, c6220 machines

## Init the 3 nodes
From my laptop, outside the cluster, I use the `./outside_cloudlab/node_init.sh` script on each node.

This scripts sets git up stuff and clones this repo on the target node; it then launches from the cloned repo from the nodes the script named confusingly: ./node_init1.sh (it can be run manually)


## Install Kubernetes
### Install Control Plane
On node0 (node where Kubernetes control plane will sit), I manually run:

`sudo ./kubernetes_init_master.sh`
Once that command is done, it prints out a join-the-cluster command, which we will use in the next step

### Join Nodes
Log into node1 and node2, and run the join-the-cluster command.

### Finish Kubernetes Control Plane Setup
Log into node0 and run 
`./kubernetes_finish_master.sh`

It'll set up kubectl config and deploy the flannel networking overlay that Kubernetes uses for pods to talk to each other.

After it's done, check that node1 and node2 are joined by running `kubectl get nodes -o wide`. 
It should look like this:
<pre>
NAME                                   STATUS   ROLES           AGE     VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
node0.srvless.sds-pg0.apt.emulab.net   Ready    control-plane   8m43s   v1.25.3   128.110.96.42    none         Ubuntu 18.04.1 LTS   4.15.0-169-generic   containerd://1.5.5
node1.srvless.sds-pg0.apt.emulab.net   Ready     none          6m15s   v1.25.3   128.110.96.43     none         Ubuntu 18.04.1 LTS   4.15.0-169-generic   containerd://1.5.5
node2.srvless.sds-pg0.apt.emulab.net   Ready     none          6m9s    v1.25.3   128.110.96.46     none        Ubuntu 18.04.1 LTS   4.15.0-169-generic   containerd://1.5.5
</pre>

Also, check that all kubernetes system pods are running with `kubectl get pods --all-namespaces` (notice 3 flannel pods running and 3 kube-proxy pods running)

<pre>
NAMESPACE      NAME                                                           READY   STATUS    RESTARTS   AGE
kube-flannel   kube-flannel-ds-c4nwb                                          1/1     Running   0          117m
kube-flannel   kube-flannel-ds-dfvfq                                          1/1     Running   0          117m
kube-flannel   kube-flannel-ds-zrt46                                          1/1     Running   0          117m
kube-system    coredns-565d847f94-jlrfw                                       1/1     Running   0          124m
kube-system    coredns-565d847f94-ttqj9                                       1/1     Running   0          124m
kube-system    etcd-node0.srvless.sds-pg0.apt.emulab.net                      1/1     Running   0          125m
kube-system    kube-apiserver-node0.srvless.sds-pg0.apt.emulab.net            1/1     Running   0          125m
kube-system    kube-controller-manager-node0.srvless.sds-pg0.apt.emulab.net   1/1     Running   0          125m
kube-system    kube-proxy-2mj7g                                               1/1     Running   0          122m
kube-system    kube-proxy-cp9sx                                               1/1     Running   0          124m
kube-system    kube-proxy-qlv8k                                               1/1     Running   0          122m
kube-system    kube-scheduler-node0.srvless.sds-pg0.apt.emulab.net            1/1     Running   0          125m
</pre>

## Install OpenWhisk
### Label Node as OpenWhisk Invoker
OpenWhisk components will be deployed using Kubernetes. OpenWhisk needs to know which node it can use to run serverless functions.
Let's label node2 as the one reserved for the invoker.
For me it looks like this (running this from kubernetes master node, node0):
`kubectl label node node2.srvless.sds-pg0.apt.emulab.net openwhisk-role=invoker`

### Add OpenWhisk to Kubernetes Cluster
`./openwhisk_init.sh` will deploy OpenWhisk; this script uses a config file `./openwhisk/mycluster.yaml ` (it's hardcoded to use that); 
**mycluster.yaml may contain interesting config changes, check that file and [lightweight docs](https://github.com/apache/openwhisk-deploy-kube/blob/master/docs/configurationChoices.md)**


Once you deploy, you need to wait about 10 minutes for all pods to launch.

Watch the exicting adventure unfold with `watch kubectl get pods -n openwhisk`

**The 'owdev-install-packages' pod might fail, it will try again and fail again a couple of times. This is fine. This seems to be some common packages people use. The error is some NodeJS package that couldn't be installed**.

When everything is done, it should look like this (notice the owdev-install-packages failures):
<pre>
NAME                                   READY   STATUS      RESTARTS   AGE
owdev-alarmprovider-674544f4f5-l5r2c   1/1     Running     0          12m
owdev-apigateway-545884988d-zhjsr      1/1     Running     0          12m
owdev-controller-0                     1/1     Running     0          12m
owdev-couchdb-c8d6c7746-4qntw          1/1     Running     0          12m
owdev-gen-certs-fkj4f                  0/1     Completed   0          12m
owdev-init-couchdb-gg8h5               0/1     Completed   0          12m
owdev-install-packages-967bg           0/1     Error       0          3m22s
owdev-install-packages-nrj9m           0/1     Error       0          12m
owdev-install-packages-sh6tg           0/1     Error       0          117s
owdev-install-packages-x2xr9           0/1     Error       0          2m41s
owdev-invoker-jzm2r                    1/1     Running     0          12m
owdev-kafka-0                          1/1     Running     0          12m
owdev-kafkaprovider-568d8b8958-sg928   1/1     Running     0          12m
owdev-nginx-749679d6f-hbhxm            1/1     Running     0          12m
owdev-redis-fb9df7bc4-74bhf            1/1     Running     0          12m
owdev-wskadmin                         1/1     Running     0          12m
owdev-zookeeper-0                      1/1     Running     0          12m
</pre>

# Experiments Workflow

### Getting Framework Metrics
While OpenWhisk collects metrics, we need to port forward the service so that it's accessible to your machine's browser:
- Run, on node0: `kubectl port-forward svc/owdev-prometheus-server 9090:9090 --namespace openwhisk` (this will forward the connection from outside the kubernetes cluster to inside the cluster, where Prometheus resides and who scrapes the metrics that OpenWhisk emits)
- From your own local machine: run `ssh -p 22 <user@node0Hostname> -L 9090:localhost:9090` (e.g. `ssh -p 22 AlexT@apt015.apt.emulab.net -L 9090:localhost:9090` ) (this will forward the connection from your machine to node0 but outside the cluster)
- In your browser: http://localhost:9090

The metrics are listed in the web interface and are described here: https://github.com/apache/openwhisk/blob/master/docs/metrics.md

To export them outside of the web interface, you can use the Prometheus API; it looks like this: `http://localhost:9090/api/v1/query?query=$METRIC` (e.g. `http://localhost:9090/api/v1/query?query=histogram_database_getDocument_finish_seconds_bucket`)


### Setup: Create Functions
Run `./openwhisk/create_basic_now_action.sh` to create 2 chains each having 3 steps/functions.

Noop Functions:
   - Action sequence called: _basics/chainnoop_
   - Each function logs its arrival, does nothing and logs departure
Spin Functions:
   - Action sequence called: _basics/chainspin_
   - Each function logs its arrival, spins for 500ms doing nothing, then logs its departure.

### Invoke Functions (Manual)

Run `./openwhisk/invoke_stepchains.sh $CHAIN_NAME`; it will invoke the chain, show the output of the chain (some json thing) and then show the logs of each of the functions
Should look something like this

<pre>
\# [...] skipped json
        "success": true
    },
    "start": 1666925939254,
    "subject": "whisk.system",
    "version": "0.0.1"
}
-- for activation id 95897563430e45d3897563430e85d3fc, log is:
2022-10-28T02:58:59.503660704Z stdout: id[9069]--step1-start
2022-10-28T02:59:00.003708239Z stdout: id[9069]--step1-end
-- for activation id 3a70ec3ac42e4705b0ec3ac42ed70578, log is:
2022-10-28T02:59:00.171686323Z stdout: id[9069]--step2-start
2022-10-28T02:59:00.671756544Z stdout: id[9069]--step2-end
-- for activation id 23c93d18378849dd893d183788d9ddf5, log is:
2022-10-28T02:59:00.831937631Z stdout: id[9069]--step3-start
2022-10-28T02:59:01.332022972Z stdout: id[9069]--step3-end

</pre>

### Invoke Functions (More Automatic)

Run:
 1. `./serverless_tooling/openwhisk/experiment_runner/running -chain $CHAIN_NAME` (there are other options, check with `-h`)
    - This tool will invoke and write the invocation results in a file by default in tmp
 2. Then run `./serverless_tooling/openwhisk/experiment_runner/process_runner_results.py` to get the results
    - This tool will read the result from the previous tool

# Observing Invocation 
On node2, the one which was labeled as invoker role earlier, the containers hosting the functions are spun up.
We can see them with (form node2):
`ctr -n moby t list`

and

`sudo docker ps`






