#!/bin/bash



echo ${SUDO_USER}
if [ "$EUID" -ne 0 ]
  then echo "Please run using sudo"
  exit
fi
set -e
set -u

echo "-------------"
echo "Expecting 3 nodes, on c6220 machines, Ubuntu 18"
echo "-------------"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cp ${SCRIPT_DIR}/.nanorc /users/${SUDO_USER}/.nanorc
cp ${SCRIPT_DIR}/.tmux.conf /users/${SUDO_USER}/.tmux.conf

chmod 777 /users/${SUDO_USER}/.nanorc

echo "--- Mounting secondary disk to /var/lib/docker so that node doesn't run out of disk ---"

sudo dmidecode | grep -A3 '^System Information'

sudo mkfs.ext4 /dev/sda4
mkdir -p /var/lib/docker/
mount -t ext4 /dev/sda4 /var/lib/docker/

hdparm -I /dev/sda | grep -A1 Model
lsblk -f
lsblk


echo "--- Installing docker, curl and other utilities; adding reposritories for kubernetes ---"

apt update
apt install -y docker
apt install -y docker.io
systemctl enable docker
apt-get install -y curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get install -y apt-transport-https ca-certificates curl
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list



sudo apt-get install git -y
apt install ncdu -y

cd /tmp
wget https://go.dev/dl/go1.19.2.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.19.2.linux-amd64.tar.gz
echo "export PATH=$PATH:/usr/local/go/bin" >> /etc/profile

sudo apt install -y python-pip
sudo apt install -y python3-pip
pip3 install python-dateutil

pip install python-dateutil
sudo apt -y  install python3-pip
pip3 install python-dateutil 

# FIX: kubelet 1.26+ needs a specific newer containerd version that doesn't exist in apt-get; need to install manually
# from: https://serverfault.com/questions/1118051/failed-to-run-kubelet-validate-service-connection-cri-v1-runtime-api-is-not-im
cd /tmp
wget https://github.com/containerd/containerd/releases/download/v1.6.12/containerd-1.6.12-linux-amd64.tar.gz
tar xvf containerd-1.6.12-linux-amd64.tar.gz
systemctl stop containerd
cd bin
cp * /usr/bin/
#####




swapoff -a
lsblk -f
lsblk
#sudo mkfs.ext4 /dev/sda4
#mkdir -p /var/lib/kubelet
#chmod 777 /bigdisk
#mount -t ext4 /dev/sda4 /var/lib/kubelet
hdparm -I /dev/sda | grep -A1 Model

# mkdir -p /bigdisk/kubelet
#chmod 777 /bigdisk/kubelet


echo "--- Installing utilities needed to compile openwhisk for dev purposes"
cd /users/AlexT/openwhisk_WASL
(cd tools/ubuntu-setup && ./all.sh)





echo "--- Installing kubelet, kubeadm, kubectl  ---"

sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
sudo dmidecode | grep -A3 '^System Information'

sudo cp /users/AlexT/serverless_tooling/config.toml /etc/containerd/config.toml
sudo mkdir -p /var/lib/docker/containerd_state
sudo mkdir -p /var/lib/docker/containerd_root


sudo systemctl start containerd
sleep 3
sudo systemctl restart containerd.service
sleep 3

/users/AlexT/serverless_tooling/compile_openwhisk.sh

cd ~
